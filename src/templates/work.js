import React from 'react'
import Slider from 'react-slick'
import { HelmetDatoCms } from '../components/helmet-dato-cms'
import Img from 'gatsby-image'
import Layout from "../components/layout"

const data = {
  "datoCmsWork": {
    "seoMetaTags": {
      "tags": [
        {
          "tagName": "title",
          "content": "Flyer 1 - Creative Inc."
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:title",
            "content": "Flyer 1"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:title",
            "content": "Flyer 1"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:site",
            "content": "@xxx"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:card",
            "content": "summary"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:modified_time",
            "content": "2016-11-23T14:48:10Z"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:published_time",
            "content": "2017-11-23T16:02:25Z"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:publisher",
            "content": "https://www.facebook.com/xxx"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:locale",
            "content": "en_EN"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:type",
            "content": "article"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:site_name",
            "content": "Creative Inc."
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:image",
            "content": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?w=1000&fit=max&fm=jpg"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:image",
            "content": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?w=1000&fit=max&fm=jpg"
          }
        }
      ]
    },
    "title": "Flyer 1",
    "excerpt": "European minnow priapumfish mosshead warbonnet shrimpfish bigscale. Cutlassfish porbeagle shark ricefish walking catfish glassfish Black swallower.",
    "gallery": [
      {
        "fluid": {
          "src": "https://www.datocms-assets.com/38678/1481206679-flyer1.jpg?auto=compress%2Cformat&fm=jpg"
        }
      },
      {
        "fluid": {
          "src": "https://www.datocms-assets.com/38678/1481207296-coffee_flyer_psd_template_by_martz90-d69fsoh.jpg?auto=compress%2Cformat&fm=jpg"
        }
      }
    ],
    "descriptionNode": {
      "childMarkdownRemark": {
        "html": "<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>\n<ol>\n<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\n<li>Aliquam tincidunt mauris eu risus.</li>\n</ol>\n<h2>Header Level 2</h2>\n<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>\n<blockquote>\n<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>\n</blockquote>\n<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>\n<ul>\n<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\n<li>Aliquam tincidunt mauris eu risus.</li>\n</ul>\n<p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek.</p>"
      }
    },
    "coverImage": {
      "url": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg",
      "fluid": {
        "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgUDhYRDhgVEhUNDhcPDQ0NFxgdGCITFhUdHysjGh0oHRUiJTUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQHC8nIig7Ozs7Oy8vLy8vNS8vLzIvLy8vLy8vLy8vLzUvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABgAEQMBIgACEQEDEQH/xAAZAAEBAQADAAAAAAAAAAAAAAAABQYBAgP/xAAgEAACAQMEAwAAAAAAAAAAAAAAAQIDBRMEESFRBhIU/8QAFwEBAQEBAAAAAAAAAAAAAAAAAgMEAP/EABsRAAICAwEAAAAAAAAAAAAAAAACARIDITER/9oADAMBAAIRAxEAPwDf3PUxptNs8462E6HDIfkWslGnvFk233Kc4bNk2bxYK4klnaTTZ0CL9j7AbDqLtTzQ2ZKpUMMeAA9NGPSyd/d9nABwD//Z",
        "aspectRatio": 0.7183098591549296,
        "src": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&fm=jpg",
        "srcSet": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.23&fm=jpg&w=663 150w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.46&fm=jpg&w=663 300w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.91&fm=jpg&w=663 600w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=663 663w",
        "sizes": "(max-width: 600px) 100vw, 600px"
      }
    }
  }
}

export default () =>{
  return (
  <Layout>
    <article className="sheet">
      <HelmetDatoCms seo={data.datoCmsWork.seoMetaTags} />
      <div className="sheet__inner">
        <h1 className="sheet__title">{data.datoCmsWork.title}</h1>
        <p className="sheet__lead">{data.datoCmsWork.excerpt}</p>
        <div className="sheet__slider">
          <Slider infinite={true} slidesToShow={2} arrows>
            {data.datoCmsWork.gallery.map(({ fluid }) => (
              <img alt={data.datoCmsWork.title} key={fluid.src} src={fluid.src} />
            ))}
          </Slider>
        </div>
        <div
          className="sheet__body"
          dangerouslySetInnerHTML={{
            __html: data.datoCmsWork.descriptionNode.childMarkdownRemark.html,
          }}
        />
        <div className="sheet__gallery">
          <Img fluid={data.datoCmsWork.coverImage.fluid} />
        </div>
      </div>
    </article>
  </Layout>
)}
