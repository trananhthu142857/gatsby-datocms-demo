import React, { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import { HelmetDatoCms } from './helmet-dato-cms';
import "../styles/index.sass";
const fake = {
  "datoCmsSite": {
    "globalSeo": {
      "siteName": "Creative Inc."
    },
    "faviconMetaTags": {
      "tags": [
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "57x57",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=57&h=57"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "60x60",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=60&h=60"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "72x72",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=72&h=72"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "76x76",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=76&h=76"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "114x114",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=114&h=114"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "120x120",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=120&h=120"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "144x144",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=144&h=144"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "152x152",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=152&h=152"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "apple-touch-icon",
            "sizes": "180x180",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=180&h=180"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "msapplication-square70x70",
            "content": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=70&h=70"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "msapplication-square150x150",
            "content": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=150&h=150"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "msapplication-square310x310",
            "content": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=310&h=310"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "msapplication-square310x150",
            "content": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=310&h=150"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "icon",
            "sizes": "16x16",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=16&h=16",
            "type": "image/png"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "icon",
            "sizes": "32x32",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=32&h=32",
            "type": "image/png"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "icon",
            "sizes": "96x96",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=96&h=96",
            "type": "image/png"
          }
        },
        {
          "tagName": "link",
          "attributes": {
            "rel": "icon",
            "sizes": "192x192",
            "href": "https://www.datocms-assets.com/38678/1479917977-Screenshot2016-11-2317.18.52.png?w=192&h=192",
            "type": "image/png"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "application-name",
            "content": "Gatsby Portfolio Website demo"
          }
        }
      ]
    }
  },
  "datoCmsHome": {
    "seoMetaTags": {
      "tags": [
        {
          "tagName": "title",
          "content": "Homepage - Creative Inc."
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:title",
            "content": "Homepage"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:title",
            "content": "Homepage"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:description",
            "content": "Pitch founders hackathon business-to-business growth hacking pivot rockstar deployment business model canvas handshake stock business-to-consumer. "
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:site",
            "content": "@xxx"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:card",
            "content": "summary"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:modified_time",
            "content": "2019-03-22T15:48:09Z"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:published_time",
            "content": "2017-01-17T08:30:21Z"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "article:publisher",
            "content": "https://www.facebook.com/xxx"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:locale",
            "content": "en_EN"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:type",
            "content": "article"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:site_name",
            "content": "Creative Inc."
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "property": "og:image",
            "content": "https://www.datocms-assets.com/38678/1479918022-Screenshot2016-11-2317.18.52.png?w=1000&fit=max&fm=jpg"
          }
        },
        {
          "tagName": "meta",
          "attributes": {
            "name": "twitter:image",
            "content": "https://www.datocms-assets.com/38678/1479918022-Screenshot2016-11-2317.18.52.png?w=1000&fit=max&fm=jpg"
          }
        }
      ]
    },
    "introTextNode": {
      "childMarkdownRemark": {
        "html": "<p>I am a creative illustrator and graphic designer with more than 10 years of experience.</p>\n<p>Originally from Toronto, currently based in London.</p>"
      }
    },
    "copyright": "©2019 Your company - Made with DatoCMS"
  },
  "allDatoCmsSocialProfile": {
    "edges": [
      {
        "node": {
          "profileType": "Facebook",
          "url": "https://www.facebook.com/xxx"
        }
      },
      {
        "node": {
          "profileType": "Twitter",
          "url": "https://twitter.com/xxx"
        }
      },
      {
        "node": {
          "profileType": "Instagram",
          "url": "https://instagram.com/xxx/"
        }
      },
      {
        "node": {
          "profileType": "Email",
          "url": "mailto:xxx@yyy.com"
        }
      }
    ]
  }
};
const TemplateWrapper = ({ children }) => {
  const [showMenu, setShowMenu] = useState(false);
  return <div className={`container ${showMenu ? "is-open" : ""}`}>
  <HelmetDatoCms
    favicon={fake.datoCmsSite.faviconMetaTags}
    seo={fake.datoCmsHome.seoMetaTags}
  />
  <div className="container__sidebar">
    <div className="sidebar">
      <h6 className="sidebar__title">
        <Link to="/">{fake.datoCmsSite.globalSeo.siteName}</Link>
      </h6>
      <div
        className="sidebar__intro"
        dangerouslySetInnerHTML={{
          __html:
            fake.datoCmsHome.introTextNode.childMarkdownRemark.html
        }}
      />
      <ul className="sidebar__menu">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>
      <p className="sidebar__social">
        {fake.allDatoCmsSocialProfile.edges.map(({ node: profile }) => (
          <a
            key={profile.profileType}
            href={profile.url}
            target="blank"
            className={`social social--${profile.profileType.toLowerCase()}`}
          >
            {" "}
          </a>
        ))}
      </p>
      <div className="sidebar__copyright">
        {fake.datoCmsHome.copyright}
      </div>
    </div>
  </div>
  <div className="container__body">
    <div className="container__mobile-header">
      <div className="mobile-header">
        <div className="mobile-header__menu">
          <button
            aria-label = "onTap"
            onClick={e => {
              e.preventDefault();
              setShowMenu(!showMenu);
            }}
          />
        </div>
        <div className="mobile-header__logo">
          <Link to="/">{fake.datoCmsSite.globalSeo.siteName}</Link>
        </div>
      </div>
    </div>
    {children}
  </div>
</div>;
};

TemplateWrapper.propTypes = {
  children: PropTypes.object
};

export default TemplateWrapper;