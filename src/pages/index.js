import React from 'react'
import { Link } from 'gatsby'
import Masonry from 'react-masonry-component'
import Img from 'gatsby-image'
import Layout from "../components/layout"

const data = {
  "allDatoCmsWork": {
    "edges": [
      {
        "node": {
          "id": "DatoCmsWork-12151765-en",
          "title": "Flyer 1",
          "slug": "flyer-1",
          "excerpt": "European minnow priapumfish mosshead warbonnet shrimpfish bigscale. Cutlassfish porbeagle shark ricefish walking catfish glassfish Black swallower.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgUDhYRDhgVEhUNDhcPDQ0NFxgdGCITFhUdHysjGh0oHRUiJTUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQHC8nIig7Ozs7Oy8vLy8vNS8vLzIvLy8vLy8vLy8vLzUvLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABgAEQMBIgACEQEDEQH/xAAZAAEBAQADAAAAAAAAAAAAAAAABQYBAgP/xAAgEAACAQMEAwAAAAAAAAAAAAAAAQIDBRMEESFRBhIU/8QAFwEBAQEBAAAAAAAAAAAAAAAAAgMEAP/EABsRAAICAwEAAAAAAAAAAAAAAAACARIDITER/9oADAMBAAIRAxEAPwDf3PUxptNs8462E6HDIfkWslGnvFk233Kc4bNk2bxYK4klnaTTZ0CL9j7AbDqLtTzQ2ZKpUMMeAA9NGPSyd/d9nABwD//Z",
              "aspectRatio": 0.7183098591549296,
              "src": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.18&fm=jpg&w=663 113w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.34&fm=jpg&w=663 225w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=0.68&fm=jpg&w=663 450w,\nhttps://www.datocms-assets.com/38678/1479912486-6lSja6y.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=663 663w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151757-en",
          "title": "Packaging 1",
          "slug": "packaging-1",
          "excerpt": "Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgLFRUWDhgNDg4SEhENFg4YFx0oGCYVFhUaHysjHR0oKRsWJDUlKC0vMjIyHiI4PTcwPCsxMi8BCgsLDg0OHBAQHDspIig7Oy87Lzw8Ly88Ly87PC8wOzU7LzUvLy87Ly81Ly8vLy82LzUvLy8vLy8vLy8vLy8vL//AABEIABgAFAMBIgACEQEDEQH/xAAZAAEAAwEBAAAAAAAAAAAAAAAABQYHBAP/xAAgEAABBAIBBQAAAAAAAAAAAAABAAIDBAURQQYSEzFR/8QAFQEBAQAAAAAAAAAAAAAAAAAAAwD/xAAaEQADAQADAAAAAAAAAAAAAAAAAQIRAxIx/9oADAMBAAIRAxEAPwDX7cwhrucVA18hG+zoHldXUsro6J7fiomFtzvvuDt+0VcvWlIkzqbNRjmaWA7RRkErvC1EmhHrm4BNVIKq1DGtjtEgcoinK0ePGWiKLUYREUAf/9k=",
              "aspectRatio": 0.8393135725429017,
              "src": "https://www.datocms-assets.com/38678/1509095906-1481207691-lovely-package-sugar-island-rum-6-e1411329470575.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509095906-1481207691-lovely-package-sugar-island-rum-6-e1411329470575.jpg?auto=compress%2Cformat&dpr=0.22&fm=jpg&w=538 113w,\nhttps://www.datocms-assets.com/38678/1509095906-1481207691-lovely-package-sugar-island-rum-6-e1411329470575.jpg?auto=compress%2Cformat&dpr=0.42&fm=jpg&w=538 225w,\nhttps://www.datocms-assets.com/38678/1509095906-1481207691-lovely-package-sugar-island-rum-6-e1411329470575.jpg?auto=compress%2Cformat&dpr=0.84&fm=jpg&w=538 450w,\nhttps://www.datocms-assets.com/38678/1509095906-1481207691-lovely-package-sugar-island-rum-6-e1411329470575.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=538 538w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151758-en",
          "title": "Stationery 1",
          "slug": "stationery-1",
          "excerpt": "User experience deployment MVP ecosystem direct mailing. Creative iteration early adopters research & development partnership buyer investor innovator success scrum project validation graphical user interface termsheet mass market.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHCwgKEg4WCAcGCBgHCAYKDhEJFgcNGhMZGBYTFhUaHysjGh0oHRUWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0PEA0OFi8dIhwvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABUAGAMBIgACEQEDEQH/xAAYAAADAQEAAAAAAAAAAAAAAAAAAgUBB//EABgQAAMBAQAAAAAAAAAAAAAAAAABAwIE/8QAFwEBAAMAAAAAAAAAAAAAAAAAAQACA//EABYRAQEBAAAAAAAAAAAAAAAAAAABEf/aAAwDAQACEQMRAD8A6Ve6YkbpE+lNC4po1qqpboTNJVKaNJiGvlISKNAaDVygAAL/2Q==",
              "aspectRatio": 1.166407465007776,
              "src": "https://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&dpr=0.16&fm=jpg&w=750 113w,\nhttps://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&dpr=0.3&fm=jpg&w=750 225w,\nhttps://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&dpr=0.6&fm=jpg&w=750 450w,\nhttps://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&dpr=0.9&fm=jpg&w=750 675w,\nhttps://www.datocms-assets.com/38678/1479910348-free-corporate-identity-mockup.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=750 750w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151759-en",
          "title": "Business card 1",
          "slug": "business-card-1",
          "excerpt": " Network effects product management interaction design paradigm shift sales accelerator equity client founders advisor bandwidth social media entrepreneur technology. ",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoTCBIPFhEXDg4NDQ0NDRsNDQ0NFxUeGBYVFhUdISsjHR0oHSEWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQHDIoIig7NC87Oy8vLy8vLy8vNS8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABIAGAMBIgACEQEDEQH/xAAYAAADAQEAAAAAAAAAAAAAAAAABAUBA//EAB4QAAICAgIDAAAAAAAAAAAAAAABAwQCETNBBSEi/8QAFwEAAwEAAAAAAAAAAAAAAAAAAgMEBf/EAB0RAAICAQUAAAAAAAAAAAAAAAACAQMRBCEiMTL/2gAMAwEAAhEDEQA/ALNSPWQ9LMsIyfVsJs62U5MPRkLiexmoe5X4wTL0qkyejRO23Fk9gHsW1y7LmRqo/srrjMAQMcgeX7MAAiivyf/Z",
              "aspectRatio": 1.3333333333333333,
              "src": "https://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=0.12&fm=jpg&w=1000 113w,\nhttps://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=0.23&fm=jpg&w=1000 225w,\nhttps://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=0.45&fm=jpg&w=1000 450w,\nhttps://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=0.68&fm=jpg&w=1000 675w,\nhttps://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=0.9&fm=jpg&w=1000 900w,\nhttps://www.datocms-assets.com/38678/1509628827-1481208020-free_business_card_mockup_psd_by_brandinhall-d5sym94copy.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1000 1000w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151766-en",
          "title": "Flyer 2",
          "slug": "flyer-2",
          "excerpt": "Deployment lean startup crowdfunding startup influencer long tail creative hackathon. Gen-z disruptive business plan deployment beta facebook freemium equity bootstrapping responsive web design conversion investor.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wEEEAAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGKEQAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGK/8IAEQgAFwAQAwEiAAIRAQMRAf/EABcAAAMBAAAAAAAAAAAAAAAAAAMFBgT/2gAIAQEAAAAArJ/Edyz/AP/EABUBAQEAAAAAAAAAAAAAAAAAAAME/9oACAECEAAAAKA//8QAFAEBAAAAAAAAAAAAAAAAAAAABP/aAAgBAxAAAAAS/wD/xAAkEAACAQIFBAMAAAAAAAAAAAABAgMEEQAFBhIhEyIxUWFx8f/aAAgBAQABPwDUNW8FIuxyjPIACPNhycZTmuZSVkUTyh1Y83AvYC/nGo6rqvGrAt0mLMq+Ah45Ps40nGHqZ5LWCCyj1u/MNpemeLvllEhHcyngn6OMnytKGKRBJv3Puva3xbH/xAAaEQACAgMAAAAAAAAAAAAAAAAAAQIRAxJB/9oACAECAQE/AIOLxNmzquH/xAAcEQABBQADAAAAAAAAAAAAAAABAAIDEiERMUH/2gAIAQMBAT8AlsJw3fOii0Wtxq//2Q==",
              "aspectRatio": 0.682219419924338,
              "src": "https://www.datocms-assets.com/38678/1509628762-1479912890-screenshot2016-11-2315-54-41.png?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509628762-1479912890-screenshot2016-11-2315-54-41.png?auto=compress%2Cformat&dpr=0.21&fm=jpg&w=541 113w,\nhttps://www.datocms-assets.com/38678/1509628762-1479912890-screenshot2016-11-2315-54-41.png?auto=compress%2Cformat&dpr=0.42&fm=jpg&w=541 225w,\nhttps://www.datocms-assets.com/38678/1509628762-1479912890-screenshot2016-11-2315-54-41.png?auto=compress%2Cformat&dpr=0.84&fm=jpg&w=541 450w,\nhttps://www.datocms-assets.com/38678/1509628762-1479912890-screenshot2016-11-2315-54-41.png?auto=compress%2Cformat&dpr=1&fm=jpg&w=541 541w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151760-en",
          "title": "Brochure 1",
          "slug": "brochure-1",
          "excerpt": "Ownership scrum project facebook handshake entrepreneur churn rate marketing traction iPhone gen-z pitch. MVP partnership conversion beta technology pitch. ",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgLFhUNDhUNDQ0ODhoODQ4OFx8ZGBYTIhUaHysjGh0oHRUWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQHDsoIigvNS87LzsvLy8vLzUvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABAAGAMBIgACEQEDEQH/xAAXAAADAQAAAAAAAAAAAAAAAAAABQYH/8QAGxAAAQUBAQAAAAAAAAAAAAAAAQACAwQFETH/xAAWAQEBAQAAAAAAAAAAAAAAAAACAwH/xAAaEQACAgMAAAAAAAAAAAAAAAAAAREhAhJB/9oADAMBAAIRAxEAPwDWbdtscZKjtXdijl4SE51JHGIgFRNvNdbsdJPqcWWwePSnytmN7QeoSinnursABQtaUhes0f/Z",
              "aspectRatio": 1.5,
              "src": "https://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=0.1&fm=jpg&w=1170 113w,\nhttps://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=0.2&fm=jpg&w=1170 225w,\nhttps://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=0.39&fm=jpg&w=1170 450w,\nhttps://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=0.58&fm=jpg&w=1170 675w,\nhttps://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=0.77&fm=jpg&w=1170 900w,\nhttps://www.datocms-assets.com/38678/1509628865-1481208578-a4-trifold-brochure-psd-mockup-01-1170x780.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1170 1170w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151761-en",
          "title": "Book 1",
          "slug": "book-1",
          "excerpt": "Ramen branding virality innovator first mover advantage iPhone MVP accelerator alpha series A financing android. Alpha MVP partnership early adopters equity social media.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgLCgoLDh8QGg0UDh0hFhEkFx8ZJSUfIh0aKysjHR0oHRUiJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQGjslIh07Oy87OzsvOy8vNTsvOzUvOy87LzsvLy8vOy87Ly8vOy87NS87LzsvLzU7Ly87Oy87O//AABEIABAAGAMBIgACEQEDEQH/xAAXAAEAAwAAAAAAAAAAAAAAAAAGAAQF/8QAHhAAAQMEAwAAAAAAAAAAAAAAAQACAwQFBhESExT/xAAVAQEBAAAAAAAAAAAAAAAAAAAEA//EABwRAAEDBQAAAAAAAAAAAAAAAAMAAQIRITEyQv/aAAwDAQACEQMRAD8Ac198ZFESUMrcxpfV1chy2m1zxwSREIa/BYHVnaWbO0YrE5ShSE261bbd2ysDgor1BjrYYwAFEqOFCUmrZf/Z",
              "aspectRatio": 1.5,
              "src": "https://www.datocms-assets.com/38678/1509090358-cover-of-the-process-manual-by-dan-ogren-34557658.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509090358-cover-of-the-process-manual-by-dan-ogren-34557658.jpg?auto=compress%2Cformat&dpr=0.19&fm=jpg&w=600 113w,\nhttps://www.datocms-assets.com/38678/1509090358-cover-of-the-process-manual-by-dan-ogren-34557658.jpg?auto=compress%2Cformat&dpr=0.38&fm=jpg&w=600 225w,\nhttps://www.datocms-assets.com/38678/1509090358-cover-of-the-process-manual-by-dan-ogren-34557658.jpg?auto=compress%2Cformat&dpr=0.75&fm=jpg&w=600 450w,\nhttps://www.datocms-assets.com/38678/1509090358-cover-of-the-process-manual-by-dan-ogren-34557658.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=600 600w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151762-en",
          "title": "Stationery 2",
          "slug": "stationery-2",
          "excerpt": "Traction virality focus assets metrics success seed money holy grail infographic. Partner network validation social media marketing niche market market seed money client long tail research & development bootstrapping.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICBMXFgoTFw4QDg0NDRENDQ0NFxUZGBYfFhUaHysjGh0oHSEWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OEBAQEi8cFhwvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABAAGAMBIgACEQEDEQH/xAAYAAACAwAAAAAAAAAAAAAAAAAABAIFBv/EAB0QAAICAQUAAAAAAAAAAAAAAAAFAgQDARESFDH/xAAUAQEAAAAAAAAAAAAAAAAAAAAC/8QAFhEBAQEAAAAAAAAAAAAAAAAAAAER/9oADAMBAAIRAxEAPwDbWXUIjK5hpn8F7KDkNLlfXFoyLDJY0jECNivvEAk//9k=",
              "aspectRatio": 1.5,
              "src": "https://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.08&fm=jpg&w=1500 113w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.15&fm=jpg&w=1500 225w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.3&fm=jpg&w=1500 450w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.45&fm=jpg&w=1500 675w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.6&fm=jpg&w=1500 900w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=0.9&fm=jpg&w=1500 1350w,\nhttps://www.datocms-assets.com/38678/1479910802-stationery-mockup-1-infinity-originalmockups.com-1500x1000-q100.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1500 1500w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151763-en",
          "title": "Web 1",
          "slug": "web-1",
          "excerpt": "Deployment seed money pitch responsive web design MVP pivot alpha. Holy grail long tail client marketing termsheet gamification channels A/B testing.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgWEhALDhUPDQ0WDhUODQ0YFxcZGBYTFhUaHysjGh0oHRUWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHRAQHDscIhw7Ly87LzUvLy8vLzUvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABAAGAMBIgACEQEDEQH/xAAXAAADAQAAAAAAAAAAAAAAAAAABAUG/8QAHBAAAgICAwAAAAAAAAAAAAAAAAEDBQIRBBIi/8QAFAEBAAAAAAAAAAAAAAAAAAAAA//EABkRAAEFAAAAAAAAAAAAAAAAAAACESEiMf/aAAwDAQACEQMRAD8A1F3mnEyZU4+mM3UmomTqnkayDqLIzeQ94mgFb2wUUTbANTOMnD//2Q==",
              "aspectRatio": 1.4995178399228544,
              "src": "https://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.08&fm=jpg&w=1555 113w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.15&fm=jpg&w=1555 225w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.29&fm=jpg&w=1555 450w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.44&fm=jpg&w=1555 675w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.58&fm=jpg&w=1555 900w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=0.87&fm=jpg&w=1555 1350w,\nhttps://www.datocms-assets.com/38678/1479910902-CareBelle.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1555 1555w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151764-en",
          "title": "Poster 1",
          "slug": "poster-1",
          "excerpt": "IPhone android client non-disclosure agreement supply chain startup buzz crowdfunding. A/B testing stock stealth beta angel investor gen-z direct mailing handshake research & development infographic strategy infrastructure. ",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wEEEAAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGKEQAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGK/8IAEQgAFAAaAwEiAAIRAQMRAf/EABkAAAIDAQAAAAAAAAAAAAAAAAQFAgMHBv/aAAgBAQAAAAA4ad6OLzPNM5F2L//EABYBAQEBAAAAAAAAAAAAAAAAAAQAA//aAAgBAhAAAAC0Gr//xAAXAQADAQAAAAAAAAAAAAAAAAAAAgME/9oACAEDEAAAABdcP//EACkQAAECBAQEBwAAAAAAAAAAAAECAwAEESEFEiIxBhRBYRMyUnGRodH/2gAIAQEAAT8Af4pZS281lPnABr03heNTj7hXmbQEgDKi9+5MHEZtolwuuKKqUC7DtYDaFY9MAkVlzQ75D+xOybDcvQVVqJNbG5r03jCkSantaglBSb17W2jGnsNVLOeE6FugaRc2qOntHOOepPxDjDakkKFREvKM84WymqFBVUnbTCJaXS4AllAsdhBlmqnT9x//xAAcEQACAQUBAAAAAAAAAAAAAAABAiEAAxESMRD/2gAIAQIBAT8ARl2XaQO0SpwViJxVrvn/xAAfEQACAgIBBQAAAAAAAAAAAAABAgAhAxESIjEyUXH/2gAIAQMBAT8AyK/EhR1EV9ipkTYf3Wxeo/iYtdp//9k=",
              "aspectRatio": 1.315736040609137,
              "src": "https://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=0.09&fm=jpg&w=1296 113w,\nhttps://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=0.18&fm=jpg&w=1296 225w,\nhttps://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=0.35&fm=jpg&w=1296 450w,\nhttps://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=0.53&fm=jpg&w=1296 675w,\nhttps://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=0.7&fm=jpg&w=1296 900w,\nhttps://www.datocms-assets.com/38678/1509095879-1479910934-screenshot2016-11-2315-21-33.png?auto=compress%2Cformat&dpr=1&fm=jpg&w=1296 1296w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151768-en",
          "title": "Brochure 2",
          "slug": "brochure-2",
          "excerpt": "Ecosystem paradigm shift validation entrepreneur stealth business-to-business vesting period churn rate customer ownership. Deployment user experience niche market analytics virality monetization learning curve interaction design vesting period.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgTFhYWDQ0NDg0VDhINDg4MFx8ZGCITFiEaHysjHR0oHRUWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OFQ4QFS8dIhwvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABAAGAMBIgACEQEDEQH/xAAZAAABBQAAAAAAAAAAAAAAAAAFAAEDBAb/xAAbEAACAwEBAQAAAAAAAAAAAAABAgADBBEhBf/EABYBAQEBAAAAAAAAAAAAAAAAAAECAP/EABYRAQEBAAAAAAAAAAAAAAAAAAABEf/aAAwDAQACEQMRAD8A2q2iyO+tc6+watxqkOh20DgM1MxZu+ilx4DFBQoao9Jikh//2Q==",
              "aspectRatio": 1.4988558352402745,
              "src": "https://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=0.09&fm=jpg&w=1310 113w,\nhttps://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=0.18&fm=jpg&w=1310 225w,\nhttps://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=0.35&fm=jpg&w=1310 450w,\nhttps://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=0.52&fm=jpg&w=1310 675w,\nhttps://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=0.69&fm=jpg&w=1310 900w,\nhttps://www.datocms-assets.com/38678/1509628707-1479910693-07_portrait_brochure_mockup.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1310 1310w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151769-en",
          "title": "Packaging 2",
          "slug": "packaging-2",
          "excerpt": "Analytics stock virality business-to-consumer ownership rockstar mass market network effects venture twitter investor paradigm shift scrum project. Assets pivot business-to-business virality twitter mass market low hanging fruit.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwkHBg8QEAoSBgcIDg0HBw0ICBEJDQ4YFxMZGBYVFiEaHysjGh0oHSEWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLDg0OHBAQHS8lIigvLy81Ly8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABEAGAMBIgACEQEDEQH/xAAYAAADAQEAAAAAAAAAAAAAAAAAAwQBAv/EABwQAAIBBQEAAAAAAAAAAAAAAAADAQIEESEyMf/EABYBAQEBAAAAAAAAAAAAAAAAAAIBA//EABkRAQACAwAAAAAAAAAAAAAAAAABIQIxQf/aAAwDAQACEQMRAD8AgezQpNWyi4RiBKl4kxvhZRbptejBlaMwYUldzySq6ABQGW1E+AAEk4f/2Q==",
              "aspectRatio": 1.4184397163120568,
              "src": "https://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=0.12&fm=jpg&w=1000 113w,\nhttps://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=0.23&fm=jpg&w=1000 225w,\nhttps://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=0.45&fm=jpg&w=1000 450w,\nhttps://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=0.68&fm=jpg&w=1000 675w,\nhttps://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=0.9&fm=jpg&w=1000 900w,\nhttps://www.datocms-assets.com/38678/1481208665-paper-bag-packaging-mockup-psd.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=1000 1000w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151770-en",
          "title": "Logo 1",
          "slug": "logo-1",
          "excerpt": "Advisor rockstar market. Incubator investor pitch business model canvas interaction design. Holy grail gen-z twitter return on investment deployment burn.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wEEEAAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGKEQAJAAsACwANABEADQASABQAFAASABgAGgAYABoAGAAkACEAHgAeACEAJAA2ACcAKQAnACkAJwA2AFEAMwA7ADMAMwA7ADMAUQBHAFYARwBCAEcAVgBHAIAAZQBZAFkAZQCAAJQAfAB2AHwAlACzAKAAoACzAOEA1QDhASUBJQGK/8IAEQgAEQAdAwEiAAIRAQMRAf/EABgAAQADAQAAAAAAAAAAAAAAAAMCBAYH/9oACAEBAAAAAOqnTyOlpSBACp//xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/aAAgBAhAAAACE/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/2gAIAQMQAAAAo//EACYQAAIBAwIEBwAAAAAAAAAAAAECAAMRIQQxBRNCYSIjQVGBkeH/2gAIAQEAAT8Ap0qKjw01HxKuuRDZmUC18nP0I2vLOoWm5BOWOAB753nEaWpo1vJBKNcgDpPqJXtUIJdgB0g2BgUAm1h3GP2cxVYnFz2jajO8aGPG3n//xAAWEQEBAQAAAAAAAAAAAAAAAAABABD/2gAIAQIBAT8Ah3//xAAWEQADAAAAAAAAAAAAAAAAAAAAEBH/2gAIAQMBAT8AI//Z",
              "aspectRatio": 1.6582064297800339,
              "src": "https://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=0.12&fm=jpg&w=980 113w,\nhttps://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=0.23&fm=jpg&w=980 225w,\nhttps://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=0.46&fm=jpg&w=980 450w,\nhttps://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=0.69&fm=jpg&w=980 675w,\nhttps://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=0.92&fm=jpg&w=980 900w,\nhttps://www.datocms-assets.com/38678/1509628904-1481208830-screenshot2016-12-0815-53-42.png?auto=compress%2Cformat&dpr=1&fm=jpg&w=980 980w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      },
      {
        "node": {
          "id": "DatoCmsWork-12151771-en",
          "title": "Poster 2",
          "slug": "poster-2",
          "excerpt": "Buyer first mover advantage stock buzz A/B testing paradigm shift. Release early adopters strategy partnership vesting period direct mailing ramen freemium series A financing focus mass.",
          "coverImage": {
            "fluid": {
              "base64": "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHBwgHBgoICAgLChIXDhMQFQ0VDRkNDhUYFxUZGBYVFhUmHysjGh0oHRYWJDUlKC0vMjIyGSI4PTcwPCsxMi8BCgsLBQ0OFQoFEC8cFhwvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL//AABEIABgAFgMBIgACEQEDEQH/xAAZAAACAwEAAAAAAAAAAAAAAAAABQQGBwH/xAAhEAABBAEDBQAAAAAAAAAAAAAAAQIDEQUEFCEGEhMxUf/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A1nJTJFDyJdPkGOkqyT1C923WvhTtDJNulu/YVdvOjks6LYnu7EsAHGU06Sx0JNPjGNkugACasCN4AAA//9k=",
              "aspectRatio": 0.8968609865470852,
              "src": "https://www.datocms-assets.com/38678/1481209112-6.jpg?auto=compress%2Cformat&fm=jpg",
              "srcSet": "https://www.datocms-assets.com/38678/1481209112-6.jpg?auto=compress%2Cformat&dpr=0.19&fm=jpg&w=600 113w,\nhttps://www.datocms-assets.com/38678/1481209112-6.jpg?auto=compress%2Cformat&dpr=0.38&fm=jpg&w=600 225w,\nhttps://www.datocms-assets.com/38678/1481209112-6.jpg?auto=compress%2Cformat&dpr=0.75&fm=jpg&w=600 450w,\nhttps://www.datocms-assets.com/38678/1481209112-6.jpg?auto=compress%2Cformat&dpr=1&fm=jpg&w=600 600w",
              "sizes": "(max-width: 450px) 100vw, 450px"
            }
          }
        }
      }
    ]
  }
}
const IndexPage = () => {
  return (
    <Layout>
      <Masonry className="showcase">
        {data.allDatoCmsWork.edges.map(({ node: work }) => (
          <div key={work.id} className="showcase__item">
            <figure className="card">
              <Link to={`/works/${work.slug}`} className="card__image">
                <Img fluid={work.coverImage.fluid} />
              </Link>
              <figcaption className="card__caption">
                <h6 className="card__title">
                  <Link to={`/works/${work.slug}`}>{work.title}</Link>
                </h6>
                <div className="card__description">
                  <p>{work.excerpt}</p>
                </div>
              </figcaption>
            </figure>
          </div>
        ))}
      </Masonry>
    </Layout>
  )}

export default IndexPage
