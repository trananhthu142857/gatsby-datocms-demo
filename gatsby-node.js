const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

const fake = {
  "allDatoCmsWork": {
    "edges": [
      {
        "node": {
          "slug": "flyer-1",
        }
      },
      {
        "node": {          
          "slug": "packaging-1",          
        }
      },
      {
        "node": {          
          "slug": "stationery-1",          
        }
      },
      {
        "node": {
          "slug": "business-card-1",
          
        }
      },
      {
        "node": {
          "slug": "flyer-2",          
        }
      },
      {
        "node": {          
          "slug": "brochure-1",          
        }
      },
      {
        "node": {          
          "slug": "book-1",          
        }
      },
      {
        "node": {         
          "slug": "stationery-2",          
        }
      },
      {
        "node": {          
          "slug": "web-1",
        }
      },
      {
        "node": {          
          "slug": "poster-1",          
        }
      },
      {
        "node": {          
          "slug": "brochure-2",          
        }
      },
      {
        "node": {
          "slug": "packaging-2",          
        }
      },
      {
        "node": {
          "slug": "logo-1",
        }
      },
      {
        "node": {
          "slug": "poster-2",
        }
      }
    ]
  }
}
exports.createPages = ({ actions }) => {
  const { createPage } = actions
  fake.allDatoCmsWork.edges.forEach(element => {
    createPage({
      path: `works/${element.node.slug}`,
      component: require.resolve(`./src/templates/work.js`),
      context: {
        slug: element.node.slug,
      },
    })
  });
}
